This is the final output of having gone through the Django tutorial at https://docs.djangoproject.com/en/1.8/intro/tutorial01/ through phase 4, and adding a little data.  It is useful for having a place one can just grab a working webapp that can be deployed in a small instance for demonstration purposes.   You still need to go through the tutorial to get Python and Django properly configured.  After that, just clone this repo and you can run it in the cloned directory with 

python manage.py runserver 0.0.0.0:8080 

for example, which will run it on port 8080 at localhost, available to the external clients (ordinarily it's only available from inside localhost, but the 0.0.0.0 IP address indicates it's OK for external too).  